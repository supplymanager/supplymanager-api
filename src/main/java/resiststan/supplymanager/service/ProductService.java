package resiststan.supplymanager.service;

import resiststan.supplymanager.doamins.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();

    Product findById(Long id);

    Product create(Product product);

    Product update(Long id, Product product);

    void delete(Product product);
}

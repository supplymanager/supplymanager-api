package resiststan.supplymanager.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import resiststan.supplymanager.doamins.Product;
import resiststan.supplymanager.excaptions.ProductNotFoundException;
import resiststan.supplymanager.repository.ProductRepository;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product findById(Long id) {
        return productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
    }

    @Override
    public Product create(Product culture) {
        return productRepository.save(culture);
    }

    @Override
    public Product update(Long id, Product product) {
        productRepository.findById(id).ifPresent(cultureFromBD ->
                BeanUtils.copyProperties(product, cultureFromBD, "id")
        );

        return productRepository.save(product);
    }

    @Override
    public void delete(Product culture) {
        productRepository.delete(culture);
    }
}

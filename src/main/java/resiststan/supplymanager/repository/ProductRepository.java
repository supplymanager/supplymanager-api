package resiststan.supplymanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import resiststan.supplymanager.doamins.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}

package resiststan.supplymanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupplyManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SupplyManagerApplication.class, args);
    }
}

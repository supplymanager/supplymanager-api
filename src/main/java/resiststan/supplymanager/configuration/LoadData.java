package resiststan.supplymanager.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import resiststan.supplymanager.doamins.Product;
import resiststan.supplymanager.service.ProductService;

@Configuration
@Slf4j
public class LoadData {
    @Bean
    CommandLineRunner initData(ProductService productService) {
        return args -> {
            for (int i = 1; i < 5; i++) {
                Product product = new Product();
                product.setName("Product " + i);
                product.setDescription("Product description " + i);
                product.setTime(i + 100L);

                productService.create(product);
                log.info("insert " + product);
            }
        };
    }
}
